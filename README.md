# Vulnerability Assessment Planner

1. Open index.html in browser (if you want to use the datepicker you'll need an internet connection)
1. Select date of first scan
1. Select number of scans to plan
1. Estimate number of days to produce VA report

![Screenshot of result](https://i.imgur.com/AuRRxXs.png "Screenshot of result")